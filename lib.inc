%define EXIT 60
%define STDOUT 1
%define SYS_CALL 1
%define NULL_TERMINATOR 0
%define MIN '0'
%define MAX '9'

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .loop:
        cmp byte[rax+rdi],NULL_TERMINATOR
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ;в rdx длина строки
    mov rsi, rdi ;в rsi адрес строки
    mov rax, 1 ;системный вызов вывода
    mov rdi, STDOUT ;дескриптор stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char: 
    push rdi
    mov rdx, STDOUT     
    mov rsi, rsp ;получаем адрес символа через указатель на стек
    pop rdi
    mov rax, SYS_CALL
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, 10 ;основание системы
    mov r9, rsp ; сохраняем указатель на стек
    mov rax, rdi
    push 0 ;'0' конец строки
    .loop:
        xor rdx, rdx
        div r8 ;целая часть в rax, остаток в rdx
        add rdx, '0' ;переводим символ в ASCII
        dec rsp
        mov byte[rsp], dl ;записываем символ из rdx по адресу rsp
        test rax, rax ;если конец выходим 
        jz .end
        jmp .loop

    .end:
        mov rdi, rsp ;записываем адрес первого символа и вызываем print_string
        push r9
        call print_string
        pop r9
        mov rsp, r9 ;восстанавливаем указатель стека
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jl .neg ;если число < 0 переход на .neg
    jmp print_uint ;если >= 0 то выводим как беззнаковое

    .neg
        push rdi  ;сохраняем число
        mov rdi, '-' ;выводим -
        call print_char
        pop rdi
        neg rdi ;берем отрицательное число
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    xor r9, r9
    .loop: ;r8b <- запись 8 bit char
        mov r8b, byte[rdi+rcx] ;r8 первая строка
        mov r9b, byte[rsi+rcx] ;r9 вторая строка
        cmp r8b,r9b 
        jne .false
        test r8b, r8b
        jz .true
        inc rcx
        jmp .loop
    
    .true:
        mov rax, 1
        ret
    .false:
        xor rax,rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ;stdin дескриптор
    xor rdi,rdi 
    mov rdx, 1
    push 0 ;кладем пустой символ на стек, теперь он по адресу [rsp]
    mov rsi, rsp ;загружаем адрес символа
    syscall
    pop rax ;загружаем сивмол со стека в rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес начала строки
; rsi - размер буфера
read_word:
    xor rcx, rcx
    xor rax, rax
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop rcx

        test rax,rax ;если конец, переход на .end
        jz .end

        cmp rax, ' ' ;проверки на пустые символы, переход на .empty_symbol
        je .empty_symbol
        cmp rax, '	'
        je .empty_symbol
        cmp rax, '\n'
        je .empty_symbol

        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi ;проверка если указатель больше длины буфера
        jge .error

        jmp .loop

    .empty_symbol:
        test rcx, rcx ;если слово еще не началась, то пропуск
        jz .loop
        jmp .end ;если уже были символы, значит этот символ - конец слова
    .error:                                                              
        xor rax, rax 
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax
        mov rax, rdi
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 ;основание системы
    .loop:
        movzx r9, byte[rdi+rcx] ;r9 это строка
        test r9, r9 ;если 0 значит конец строки
        jz .end
        cmp r9b, MIN ;если > 0 то возможно цифра
        jl .end ;если не цифра, прекращаем.
        cmp r9b, MAX ;если < 9 то цифра
        jg .end

        mul r8 
        sub r9b, '0' ;получаем цифру,  вычитая из кода символа код 0
        add rax, r9 ;двигаем на рязряд и добавляем цифру
        inc rcx
        jmp .loop

    .end
        mov rdx, rcx ;записываем в rdx длину
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi 
    cmp byte[rcx], '-' ;если '-' переход на .neg
    je .neg
    jmp .pos ;иначе на .pos
    .neg:
        inc rcx ;чтобы убрать минус
        mov rdi, rcx
        push rcx
        call parse_uint
        pop rcx
        neg rax
        inc rdx
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax ;r8 это длина строки
    cmp rdx, r8 ;если длина буфера меньше длины строки значит не ошибка
    jl .error
    .loop:
        cmp rcx, r8 ;если указатель больше длины строки то заканчиваем
        jg .end
        mov r10,[rdi+rcx]
        mov [rsi+rcx], r10
        inc rcx
        jmp .loop
    
    .error:
        xor rax, rax
        ret
    .end:
        mov rax, r8
        ret
